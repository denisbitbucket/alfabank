import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ParseAndSort {


    public static void main(String[] args) throws FileNotFoundException {
        String path = "src/file.txt";
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        String line = scanner.nextLine();
        String [] data = line.split(",");
        Integer numbers [] = new Integer[21];
        int counter = 0;
        for (String nums : data){
            numbers[counter++] = Integer.parseInt(nums);
        }
        Integer [] numbersClone = numbers.clone();
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbersClone,Collections.reverseOrder());
        System.out.println(Arrays.toString(numbersClone));
    }

}
