import Alfa.Helpers;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static java.lang.Thread.sleep;


public class YandexMarket extends Helpers {

    @Test
    public void yandexMarketMobile() throws InterruptedException {
        goToMarketElectronic();
        chooseMobile();
        setPriceFilter("40000", "");
        sleep(3000);
        List<WebElement> webElements = wd.findElements(By.cssSelector("a[class^='n-snippet-cell2__image']"));
        WebElement firstElement = (WebElement) Iterables.getFirst(webElements, wd.findElements(By.cssSelector("a[class^='n-snippet-cell2__image']")));
        String name = firstElement.getAttribute("title");
        String href = firstElement.getAttribute("href");
        wd.get(href);
        System.out.println(name);
        System.out.println(href);
        String mobNameFromSpec = wd.findElement(By.cssSelector("h1.title")).getText();
        Assert.assertEquals(name,mobNameFromSpec);


    }

    @Test
    public void yandexMarketEars() {
        goToMarketElectronic();
        chooseEars();
        //wd.findElement(By.xpath("//label[@for='7893318_8455647']//span[.='Beats']")).click();
        wd.findElement(By.cssSelector("div[class='LhMupC0dLR']")).click();
        List<WebElement> webElements = wd.findElements(By.cssSelector("a[class^='n-snippet-cell2__image']"));
        System.out.println(webElements.size());
        /*String earName = wd.findElement(By.linkText("Наушники Beats Solo3 Wireless")).getText();
        wd.get("https://market.yandex.ru/product/1713772584?show-uid=328060989079968919316001&nid=56179&glfilter=7893318%3A8455647&pricefrom=17000&priceto=25000&context=search");
        String earNameFromSpec = wd.findElement(By.cssSelector("h1.title")).getTex
        System.out.println(earName);
        System.out.println(earNameFromSpec);
        Assert.assertEquals(earNameFromSpec,earName);*/
    }
}