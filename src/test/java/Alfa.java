import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Alfa extends Helpers {

    @Test
    public void getInformation() throws IOException {
        Actions action = new Actions(wd);
        searchSystemName = wd.getTitle();
        yandexSearch("альфа банк1");
        wd.findElement(By.linkText("AlfaBank.ru")).click();
        catchNewWindow();
        //action.moveToElement(wd.findElement(By.linkText("Вакансии"))).click();
        wd.findElement(By.linkText("Вакансии")).click();
        wd.findElement(By.linkText("О РАБОТЕ В БАНКЕ")).click();
        String header = wd.findElement(By.cssSelector("div.message")).getText();
        String body = wd.findElement(By.cssSelector("div.info")).getText();
        BufferedWriter output = null;
        try {
            File file = new File("src/" + dateTime + "_" + browserName + "_" + searchSystemName + ".txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(header);
            output.write(paragraph);
            output.write(body);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                output.close();
            }

        }

    }

}