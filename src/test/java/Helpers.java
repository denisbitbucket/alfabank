import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Helpers {
    WebDriver wd;
    public String browserName;
    String dateTime;
    String webBrowser;
    String paragraph = "\n";
    String searchSystemName;

    @BeforeMethod
    public void setUp() {

        webBrowser = "chrome";

        if (webBrowser == "chrome") {
            wd = new ChromeDriver();
        } else {
            wd = new FirefoxDriver(new FirefoxOptions().setLegacy(true));
            wd = new FirefoxDriver();

        }
        wd.manage().window().maximize();
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        wd.get("https://yandex.ru/");
        Capabilities cap = ((RemoteWebDriver) wd).getCapabilities();
        browserName = cap.getBrowserName();
        dateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());


    }

    protected void chooseMobile() {
        wd.findElement(By.linkText("Мобильные телефоны")).click();
    }

    protected void chooseEars() {
        wd.findElement(By.linkText("Наушники и Bluetooth-гарнитуры")).click();
    }

    protected void setPriceFilter(String priceFrom, String priceTo) {
        wd.findElement(By.id("glpricefrom")).click();
        wd.findElement(By.id("glpricefrom")).clear();
        wd.findElement(By.id("glpricefrom")).sendKeys(priceFrom);
        wd.findElement(By.id("glpriceto")).click();
        wd.findElement(By.id("glpriceto")).clear();
        wd.findElement(By.id("glpriceto")).sendKeys(priceTo);
    }


    protected void goToMarketElectronic() {
        wd.findElement(By.linkText("Маркет")).click();
        wd.findElement(By.linkText("Электроника")).click();
    }


    protected void catchNewWindow() {
        String mainWindow = wd.getWindowHandle();
        Set<String> h = wd.getWindowHandles();
        Iterator<String> i = h.iterator();
        while (i.hasNext()) {
            String alterWindow = i.next();
            if (!mainWindow.equalsIgnoreCase(alterWindow)) {
                wd.switchTo().window(alterWindow);
            }
        }
    }

    protected void yandexSearch(String toSearch) {
        wd.findElement(By.id("text")).click();
        wd.findElement(By.id("text")).clear();
        wd.findElement(By.id("text")).sendKeys(toSearch);
        //wd.findElement(By.xpath("//div[@class='search2__button']//button[.='Найти']")).click();
        wd.findElement(By.cssSelector("div [class='search2__button']")).click();
    }

    @AfterMethod
    public void tearDown() {
        wd.quit();
    }
}
